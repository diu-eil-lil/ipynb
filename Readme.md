DIU Enseigner l'informatique au lycée
=====================================

Univ. Lille

Partage de notebook Jupyter pour accès via le serveur https://jupyter.fil.univ-lille1.fr 

Voir aussi le portail (public) du DIU Enseigner l'informatique au
lycée, Université de Lille,
[gitlab-fil.univ-lille.fr/diu-eil-lil/portail](https://gitlab-fil.univ-lille.fr/diu-eil-lil/portail/blob/master/Readme.md).


